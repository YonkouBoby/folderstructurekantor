pipeline {
    agent any

    parameters {
        string(name: 'PROJECT_NAME', description: 'Name of the new GitLab project')
        string(name: 'NAMESPACE', description: 'Namespace or group where the project will be created')
        string(name: 'VISIBILITY', description: 'Visibility level of the project: public, internal, or private')
        string(name: 'GROUP_NAME',description: 'Group Name')
    }

    stages {
        stage('Create GitLab Project and Generate Files') {
            steps {
                script {
                    def gitlabApiUrl = env.GITLAB_API_URL
                    def gitlabToken = env.GITLAB_TOKEN

                    // Check if GitLab API URL and token are provided
                    if (!gitlabApiUrl || !gitlabToken) {
                        error('GitLab API URL or token is not provided.')
                    }

                    //create Group
                    def createGroup = sh(script: """
                        curl -X POST \
                        --header 'Content-Type: application/json' \
                        --header 'Authorization: Bearer ${gitlabToken}' \
                        --data 'name=\${params.PROJECT_NAME}&path=\${params.GROUP_NAME}\${params.PROJECT_NAME}' \
                        '${gitlabApiUrl}/groups'
                    """, returnStdout: true).trim()

                    echo "responseGrup: $createGroup"

                    def release = sh(script: """
                        curl -X POST \
                        --header 'Content-Type: application/json' \
                        --header 'Authorization: Bearer ${gitlabToken}' \
                        --data 'name=\release`&path=\${params.GROUP_NAME}/release' \
                        '${gitlabApiUrl}/groups'
                    """, returnStdout: true).trim()

                    echo "Responserelease: $release"

                    // Create GitLab project
                    def createProjectResponse = sh (
                        script: """
                            curl -X POST \
                            --header 'Content-Type: application/json' \
                            --header 'Authorization: Bearer ${gitlabToken}' \
                            --data '{"name": "${params.PROJECT_NAME}", "namespace_id": "84047567", "visibility": "${params.VISIBILITY}"}' \
                            '${gitlabApiUrl}/projects'
                        """,
                        returnStdout: true
                    ).trim()

                    // Handle response to get project ID
                    def projectId = createProjectResponse =~ /"id":(\d+)/
                    if (projectId) {
                        projectId = projectId[0][1]
                        echo "GitLab Project Created with ID: ${projectId}"

                        // Generate files in the repository
                        def repositoryName = params.PROJECT_NAME.replaceAll(' ', '-')
                        def fileContents = ['file1.txt': 'Content for file 1', 'file2.txt': 'Content for file 2', 'file3.txt': 'Content for file 3']
                        
                        fileContents.each { filename, content ->
                            def createFileResponse = sh (
                                script: """
                                    curl -X POST \
                                    --header 'Content-Type: application/json' \
                                    --header 'Authorization: Bearer ${gitlabToken}' \
                                    --data '{"branch": "main", "content": "${content}", "commit_message": "Create ${filename}"}' \
                                    '${gitlabApiUrl}/projects/${projectId}/repository/files/${URLEncoder.encode(filename, 'UTF-8')}'
                                """,
                                returnStdout: true
                            ).trim()
                            echo "File ${filename} created successfully"
                        }
                    } else {
                        error "Failed to create GitLab project: ${createProjectResponse}"
                    }
                }
            }
        }
    }
}
